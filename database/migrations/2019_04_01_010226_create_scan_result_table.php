<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScanResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scan_result', function (Blueprint $table) {
            $table->bigIncrements('scan_result_id');
            $table->timestamps();
            $table->char('file_name_source')->comment('имя файла исходное');
            $table->char('file_name',40)->comment('имя файла в хранилище');
            $table->char('needle', 2)->comment('символ для поиска');
            $table->mediumText('result')->comment('результат поиска');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scan_result');
    }
}
