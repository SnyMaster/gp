@extends('layouts.app')

@section('content')

    <div class="page-header">
      <h1>Поиск в файле</h1>
    </div>
    @includeWhen(isset($errorMessage), 'common.error')
    @include('scan.form')
    @includeWhen(isset($result), 'scan.result')
@endsection