@include('common.errors')
<form  method="POST" action="{{ action('ScanFileController@scan') }}"enctype="multipart/form-data">
{{ csrf_field() }}        
    <div class="form-group">
      <label for="inputNumber">Число для поиска (от 1 до 9)</label>
      <input id="inputNumber" name="scanfor" type="text" class="form-control" placeholder="Число" value="{{ old('scanfor') }}">
    </div>
    <div class="form-group">
      <label for="inputFile">Файл для проверки</label>
      <input name="filename" type="file" id="inputFile">
    </div>
    <button type="submit" class="btn btn-default">Поиск</button>
</form>
