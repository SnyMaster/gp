@extends('layouts.app')

@section('content')

    <div class="page-header">
      <h1>Калькулятор валют</h1>
    </div>
    @includeWhen(isset($errorMessage), 'common.error')
    @include ('currency.form')
    @includeWhen(isset($rub), 'currency.result')
    
@endsection