@include('common.errors')
<form  method="POST" action="{{ action('CurrencyController@exchange') }}" >
{{ csrf_field() }}        
    <div class="form-group">
      <label for="inputNumber">Сумма для обмена</label>
      <input id="inputNumber" name="amount" type="text" class="form-control" placeholder="Сумма для обмена" value="<?= old('amount') ?>">
    </div>
    <div class="form-group">
      <label for="inputNumber">Валюта</label>
      <input id="inputNumber" name="currency" type="text" class="form-control" placeholder="Код валюты" value="<?= old('currency') ?>">
    </div>    
    <button type="submit" class="btn btn-default">Расчитать</button>
</form>