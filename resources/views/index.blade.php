@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row mb-2">
        <div class="col-md-6">
          <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
            <div class="col p-4 d-flex flex-column position-static">
              <h3 class="mb-0">Поиск в файле</h3>
              <div class="mb-1 text-muted">Что делает :</div>
              <p class="card-text mb-auto">Ищет в файле все числа в которых встречается заданная цифра</p>
              <a href="{{ action('ScanFileController@index') }}" class="stretched-link">Поищем...</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
            <div class="col p-4 d-flex flex-column position-static">
              <h3 class="mb-0">Калькулятор</h3>
              <div class="mb-1 text-muted">Что делает:</div>
              <p class="mb-auto">Калькулятор пересчёта валют в рубли</p>
              <a href="{{ action('CurrencyController@index') }}" class="stretched-link">Посчитаем...</a>
            </div>
          </div>
        </div>
    </div> 
</div>

@endsection