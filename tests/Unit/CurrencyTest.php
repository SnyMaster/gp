<?php

namespace Tests\Unit;

use Tests\TestCase;

class CurrencyTest extends TestCase
{
   
    public function testIndexPage()
    {
        $response = $this->get('/currency');
        $response->assertStatus(200);
        $this->assertRegExp('/(<form(.)*>(.)*<\/form>){1}/s', $response->getContent());
        $this->assertRegExp('/(<input(.)*(type="text"|name="currency")(.)*(type="text"|name="currency")(.)*>){1}/', $response->getContent());
        $this->assertRegExp('/(<input(.)*(type="text"|name="amount")(.)*(type="text"|name="amount")(.)*>){1}/', $response->getContent());
        $this->assertRegExp('/(<button(.)*type="submit"(.)*>(.)*<\/button>){1}/', $response->getContent());
    }
    
    public function testPost()
    {
        
        $response = $this->post('/currency', ['currency' => 'usd']);
        $response->assertStatus(302);
        $response->assertSessionHasErrors('amount');
        $response->assertSessionDoesntHaveErrors('currency');
        
        $response = $this->post('/currency', ['amount' => 100]);      
        $response->assertStatus(302);
        $response->assertSessionHasErrors('currency');
        $response->assertSessionDoesntHaveErrors('amount');
        
        $response = $this->post('/currency', ['currency' => 'u', 'amount' => -1]);       
        $response->assertStatus(302);
        $response->assertSessionHasErrors('amount');
        $response->assertSessionHasErrors('currency');
        
        $response = $this->post('/currency', ['currency' => 'usds']);
        $response->assertStatus(302);
        $response->assertSessionHasErrors('amount');
        $response->assertSessionHasErrors('currency');
        
    }
    
}
