<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use App\Http\Requests\ScanFileForm;
use Illuminate\Support\Facades\Validator;

class ScanRequestFormTest extends TestCase
{
    private $request;
    private $validator;
    
    public function setUp(): void {
        parent::setUp();
        $this->request = new ScanFileForm();
        $this->validator = Validator::make([], $this->request->rules());
        
    }
    public function testFormCorrectDataRequest()
    {
        $file = UploadedFile::fake()->create('file.txt');
        $this->request->replace(['scanfor' => 4, 'filename' => $file]);
        $this->validator->setData($this->request->all());

        self::assertTrue($this->validator->passes());            
    }
    
    public function testEmptyAttributeValues()
    {
        $this->request->replace(['scanfor' => '', 'filename' => '']);
        $this->validator->setData($this->request->all());
        $this->assertFalse($this->validator->passes());
        $this->assertArrayHasKey('scanfor', $this->validator->failed());
        $this->assertArrayHasKey('Required', $this->validator->failed()['scanfor']);
        $this->assertArrayHasKey('filename', $this->validator->failed());
        $this->assertArrayHasKey('Required', $this->validator->failed()['filename']);
    }
    
        
    public function testWrongValuesScanFor()
    {
        $this->request->replace(['scanfor' => 'string', 'filename' => 'string']);
        $this->validator->setData($this->request->all());
   
        $this->assertFalse($this->validator->passes());
        $this->assertArrayHasKey('scanfor', $this->validator->failed());
        $this->assertArrayHasKey('Numeric', $this->validator->failed()['scanfor']);
        $this->assertArrayHasKey('filename', $this->validator->failed());
        $this->assertArrayHasKey('File', $this->validator->failed()['filename']);
    }
    
    public function testTooBigScanFor()
    {
        $this->request->replace(['scanfor' => 10]);
        $this->validator->setData($this->request->all());
   
        $this->assertFalse($this->validator->passes());
        $this->assertTrue($this->validator->errors()->has('scanfor'));
    }    
    
    public function testTooSmallScanFor()
    {
        $this->request->replace(['scanfor' => 0]);
        $this->validator->setData($this->request->all());
   
        $this->assertFalse($this->validator->passes());
        $this->assertTrue($this->validator->errors()->has('scanfor'));       
    }        
}
