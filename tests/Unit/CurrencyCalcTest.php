<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Helpers\CurrencyCalc;

class CurrencyCalcTest extends TestCase
{
    public function testCreate()
    {
        $result = (new CurrencyCalc(10, 1, 1))->calc();
        $this->assertEquals(10, $result);
        $result = (new CurrencyCalc(100, 20, 1))->calc();
        $this->assertEquals(5, $result);
    }
    
    public function testFormatResult()
    {
        $result = (new CurrencyCalc(100, 20, 1))->calcFormat('Руб.');
        $this->assertEquals('5 Руб.', $result);        
        
        $result = (new CurrencyCalc(102, 20, 1))->calcFormat('Руб.');
        $this->assertEquals('5.1 Руб.', $result);            }
    
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testExeption()
    {
        $result = new CurrencyCalc(-10, 1, 1);
        $result = new CurrencyCalc(0, 1, 1);
        
        
        $result = new CurrencyCalc(10, -1, 1);       
        $result = new CurrencyCalc(10, 0, 0);
        
        $result = new CurrencyCalc(10, 1, -1);
    }    
}
