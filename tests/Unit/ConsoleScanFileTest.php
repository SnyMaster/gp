<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Storage;

class ConsoleScanFileTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCommand()
    {

        Storage::fake()->put('file.txt', '435 45 67');

        $this->artisan('scanfile')
                ->expectsQuestion('Число от 1 до 9 для поиска:', '4')
                ->expectsQuestion('Полний путь к файлу для поиска числа:', base_path('/storage/framework/testing/disks/local/') . 'file.txt')
                ->expectsOutput('435,45')
                ->assertExitCode(0);
           Storage::fake()->delete('file.txt');
    }
}
