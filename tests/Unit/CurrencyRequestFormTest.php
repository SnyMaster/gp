<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\Http\Requests\CurrenctFormData;
use Illuminate\Support\Facades\Validator;
use App\Services\Currency\CurrencyReaderProxy;

class CurrencyRequestFormTest extends TestCase
{
    protected $request;
    protected $validator;
    
    public function setUp(): void 
    {
        parent::setUp();
        $this->request = new CurrenctFormData();        
        $readerCurrency = $this->mock(CurrencyReaderProxy::class, function($mosk) {
            $mosk->shouldReceive('currency')->andReturn(['USD' => ['nominal' => 1, 'value' => 100]]);
        });
        
        $this->app->instance('App\Services\Currency\CurrencyReaderInterface', $readerCurrency);
        $this->validator = Validator::make([], $this->request->rules());
    }
    
    public function tearDown(): void 
    {
        parent::tearDown();
        \Mockery::close();
    }
    
    public function testCorrectValues()
    {
        $this->request->replace(['amount' => 100, 'currency' => 'USD']);
        $this->validator->setData($this->request->all());
        
        $this->assertTrue( $this->validator->passes());
        $data = $this->validator->validated();
        $this->assertArrayHasKey('currency', $data);
        $this->assertArrayHasKey('amount', $data);
    }
    
    public function testEmptyValues() 
    {
        $this->request->replace(['amount' => '', 'currency' => '']);
        $this->validator->setData($this->request->all());
        
        $this->assertFalse($this->validator->passes());        
        $this->assertArrayHasKey('amount', $this->validator->failed());
        $this->assertArrayHasKey('Required', $this->validator->failed()['amount']);
        $this->assertArrayHasKey('currency', $this->validator->failed());
        $this->assertArrayHasKey('Required', $this->validator->failed()['currency']);
    }
    
    public function testZeroValueAmount() 
    {
        $this->request->replace(['amount' => 0]);       
        $this->validator->setData($this->request->all());
        
        $this->assertFalse($this->validator->passes());               
        $this->assertArrayHasKey('amount', $this->validator->failed());
        $this->assertArrayHasKey('Min', $this->validator->failed()['amount']);
    }
    
    public function testStringValueAmount() 
    { 
        $this->request->replace(['amount' => 'string']);
        $this->validator->setData($this->request->all());
                
        $this->assertFalse($this->validator->passes());        
        $this->assertArrayHasKey('amount', $this->validator->failed());
        $this->assertArrayHasKey('Numeric', $this->validator->failed()['amount']);        
    }
    
    public function testNumberValueCurrency() 
    {
        $this->request->replace(['currency' => 100]);       
        $this->validator->setData($this->request->all());
                
        $this->assertFalse($this->validator->passes());        
        $this->assertArrayHasKey('currency', $this->validator->failed());
        $this->assertArrayHasKey('String', $this->validator->failed()['currency']);        
    }        
     
    public function testBadSizeCurrency()
    {
        $this->request->replace(['currency' => 'longString']);       
        $this->validator->setData($this->request->all());
                
        $this->assertFalse($this->validator->passes());        
        $this->assertArrayHasKey('currency', $this->validator->failed());
        $this->assertArrayHasKey('Size', $this->validator->failed()['currency']);        
    }
    
    public function testBadCodeCurrncy() 
    {
        $this->request->replace(['currency' => 'XXX']);       
        $this->validator->setData($this->request->all());
                
        $this->assertFalse($this->validator->passes());        
    }
}
