<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Middleware\UpperCase;
use App\Http\Middleware\RoundCurrency;
use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MiddleWareTest extends TestCase
{
    protected $request;
    
    public function setUp(): void {
        parent::setUp();
        $this->request = new FormRequest();        
    }
        
    public function testUpperCase()
    {
        $this->request->replace([
            'currency' => 'usd',
            'title' => 'Title test',
            'char' => '$'
            ]);
        
        $upperCase = new UpperCase();
        $upperCase->handle($this->request, function($request){
            $this->assertEquals('usd', $request->currency);
            $this->assertEquals('Title test', $request->title);
            $this->assertEquals('$', $request->char);
        });
        $upperCase->handle($this->request, function($request){
                $this->assertEquals('USD', $request->currency);
                $this->assertEquals('TITLE TEST', $request->title);
                $this->assertEquals('$', $request->char);
            }, 'currency', 'title', 'char');
    }
    
    public function testRoundCurrency()
    {
        $this->request->replace([
            'amount_1' => 100,
            'amount_2' => 100.3,            
            'amount_3' => 100.34,
            'amount_4' => 100.345,
            'amount_5' => 100.34055555
            ]);
        $roundCurrency = new RoundCurrency();
        $roundCurrency->handle($this->request, function($request){
            $this->assertEquals(100, $request->amount_1);
            $this->assertEquals(100.3, $request->amount_2);
            $this->assertEquals(100.34, $request->amount_3);
            $this->assertEquals(100.345, $request->amount_4);
            $this->assertEquals(100.34055555, $request->amount_5);
        });
    }
}
