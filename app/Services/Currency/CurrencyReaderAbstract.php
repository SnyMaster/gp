<?php
 namespace App\Services\Currency;
 
 abstract class CurrencyReaderAbstract Implements CurrencyReaderInterface
 {
    protected $url;
     
    public function setUrl(string $url) : CurrencyReaderInterface
    {
        $this->url = $url;
        return $this;
    }
    
    public  function getUrl() : string
    {
        return $this->url;
    }
 }
