<?php

namespace App\Services\Currency;

Interface CurrencyReaderInterface
{   
    public function currency() : array;
    public function setUrl(string $url) :CurrencyReaderInterface;
    public function getUrl() : string;
}
