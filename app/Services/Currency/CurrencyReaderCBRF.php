<?php

namespace App\Services\Currency;

class CurrencyReaderCBRF extends CurrencyReaderAbstract
{
    protected $url = 'https://www.cbr-xml-daily.ru/daily_utf8.xml';
    
    public function currency() : array
    {
        try {
            $source = file_get_contents($this->url);
            if ($source === false) {
                throw new \Exception('Ошибка при подклчючении к источнику курсов валют', 404);
            }
        } catch (\Exception $e) {
            throw new \Exception('Ошибка при подклчючении к источнику курсов валют', 404);
        }
        $xml = simpleXML_load_file($this->url,"SimpleXMLElement",LIBXML_NOCDATA);
        if($xml ===  FALSE) {
           throw new \Exception('Ошибка при получении данных о курсах валют');
        }
        $currencyList = [];
        foreach($xml->Valute as $valute) {
            $currencyList[(string)$valute->CharCode] = ['nominal' => (string)$valute->Nominal, 'value' => str_replace(',', '.', (string)$valute->Value)];
        }
        return $currencyList;
    }
}
