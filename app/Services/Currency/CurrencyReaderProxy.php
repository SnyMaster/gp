<?php

namespace App\Services\Currency;

use App\Services\Currency\CurrencyReaderInterface;
use App\Services\Currency\CurrencyReaderAbstract;

class CurrencyReaderProxy extends CurrencyReaderAbstract
{
    const REFRESH_TIMEOUT_DEFAULT = 1800;
    
    private $reader;
    private $currencyStorage;
    private $storageFile;
    private $refreshTimeOut;
    
    public function __construct(CurrencyReaderInterface $reader, string $storagePath, ?int $refreshTimeOut) {
        $this->reader = $reader;   
        $this->storageFile = $storagePath . '/currency.json';
        $this->refreshTimeOut = $refreshTimeOut ? $refreshTimeOut : self::REFRESH_TIMEOUT_DEFAULT;
    }
    
    public function currency(): array {
        if (!$this->currencyStorage && 
                !$this->currencyStorage = $this->readCurrencyStorage()) {
            if ($this->url) {
                $this->reader->setUrl($this->url);
            }
            $this->currencyStorage  = $this->reader->currency();
            $this->saveCurrencyStorage(json_encode($this->currencyStorage));
        }
        return $this->currencyStorage;
    }
    
    private function readCurrencyStorage()
    {
        if (file_exists($this->storageFile) && (filemtime($this->storageFile) >= time() - $this->refreshTimeOut)) {
            return json_decode(file_get_contents($this->storageFile), true);
        }
        return false;
    }
    
    private function saveCurrencyStorage(string $dataToSave)
    {
        if ( false === file_put_contents($this->storageFile, $dataToSave)) {
            throw new Exception("Ошибка сохранения файла '$this->storageFile'");
        }
    }
}