<?php

namespace App\Services\Scan;

use Illuminate\Support\Facades\File;

abstract class FileScaner
{
    private $file;
    
    public function setFile(string $fileName) 
    {    
        $this->file = $fileName;
        $this->checkFile();
        return $this;
    }
    
    /**
     * Проверяет задано ли имя файла и его существование файла
     * 
     * @throws \InvalidArgumentException
     * @throws \Exeption
     */
    private function checkFile()
    {
        if (empty($this->file)) {
            throw new \Exeption('Не задан файл для обработки.');
        }
        if (!File::isFile($this->file)) {
            throw new \InvalidArgumentException("Не удалось обнаружить файл по указанному пути.");
        }
        if (!File::isReadable($this->file)) {
            throw new \Exeption('Ошибка доступа к файлу.');
        }
        
    }
    
    /**
     * Чтение файла посимвольно
     * 
     * @throws \Exeption
     */
    public function read()
    {
        $this->checkFile();
        $handle = fopen($this->file, 'rb');
        if ($handle === false) {
            throw new \Exeption('Ошибка доступа к файлу.');
        }

        $mbch = '';    // keeps the first byte of 2-byte cyrillic letters
        while (FALSE !== ($ch = fgetc($handle))) {       
            //check for the sign of 2-byte cyrillic letters   
            if (empty($mbch) && (FALSE !== array_search(ord($ch), Array(208,209,129)))) {
                $mbch = $ch;    // keep the first byte
                continue;
            }
            yield $mbch . $ch;
            if (!empty($mbch)) { 
                $mbch = '';    // erase the byte after using
            }
        }
    }
    
    /**
     * Проверка файла
     * 
     */
    abstract public function scan($needle);
}
