<?php

namespace App\Services\Scan;

use App\Services\Scan\FileScaner;

class FileScanerNumber extends FileScaner
{
    /**
     * Проверяет наличие в файле чисел включающих заданное 
     * числа отделено пробелами от нечисел
     * 
     * @param type $needle число для поиска
     * 
     * @return array
     */
    public function scan($needle)
    {
        $result = [];
        $block = [];
        foreach($this->read() as $char) {
            if (intval($char)) {
              $block[] = $char; 
            } elseif ($char === ' ' && !empty($block) && in_array($needle, $block)) {
                $result[] = implode('', $block);
                $block = [];
            } else {
                $block = [];
            }
        }
        usort($result, function($el1, $el2){ return strlen($el1) < strlen($el2) ? 1 : -1; });
        return $result;
    }
}
