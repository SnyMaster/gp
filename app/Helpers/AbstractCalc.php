<?php

namespace App\Helpers;

abstract class AbstractCalc
{
    protected $value;
    protected $nominal;
    protected $amount;
    
    /**
     * 
     * @param float $value  курс обмена
     * @param int $nominal для какой суммы исходной валюты указан курс
     * @param float $amount сумма для пересчёта
     * 
     * @throws InvalidArgumentException
     */
    public function __construct(float $value, int $nominal, float $amount) {
        if ($value <= 0) {
            throw new \InvalidArgumentException('Значение курса не может быть равным нулю или отрицательным числом.');
        }
        if ($nominal <= 0) {
            throw new \InvalidArgumentException('Значение  номинала не может быть равным нулю или отрицательным числом.');
        }
        if ($amount < 0) {
            throw new \InvalidArgumentException('Значение суммы не может быть отрицательным числом.');
        }
        $this->value = $value;
        $this->nominal = $nominal;
        $this->amount = $amount;
    }
    
    abstract public function calc();
    
    abstract public function calcFormat(string $nameDesctinationValute) : string;
    
}
