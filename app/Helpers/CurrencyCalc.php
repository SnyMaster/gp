<?php

namespace App\Helpers;

class CurrencyCalc extends AbstractCalc
{
    public function calc()
    {
       $result = round(($this->amount * $this->value / $this->nominal), 2); 
       
       return $result;
    }
    
    public function calcFormat(string $nameDesctinationValute) : string 
    {
        return $this->calc() . ' ' . $nameDesctinationValute; 
    }
    
}
