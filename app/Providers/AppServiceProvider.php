<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Services\Currency\CurrencyReaderInterface', function () {
            $params = config('services.currency');
            if (empty($params['reader'])) {
                throw new \Exception('Не заданы параметры для сервиса получения данных по валютам в файле конфигурации');
            }
            if (empty($params['storagePath'])) {
                throw new \Exception('Не задана путь для хранения данных о курсах');
            }

            return new \App\Services\Currency\CurrencyReaderProxy(
                    new $params['reader'](), 
                    $params['storagePath'], 
                    (isset($params['refreshTimeOut']) ? $params['refreshTimeOut'] : null));
            
        });
        
        $this->app->singleton('App\Services\Scan\FileScaner', function () {
            return new \App\Services\Scan\FileScanerNumber();
        });
        $this->app->bind('Calc',\App\Helpers\CurrencyCalc::class);
    }
    
}
