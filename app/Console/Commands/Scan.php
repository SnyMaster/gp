<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator as Vld;
use Illuminate\Support\Facades\File;
use App\Services\Models\ScanResult;
use Illuminate\Contracts\Validation\Validator;
use App\Services\Scan\FileScaner;

class Scan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scanfile '
            . '{--S|scan-for= : число от 1 до 9 для поиска в файле} '
            . '{--F|file-name= : полный путь к файл в котором происходит поиск заданного параметра}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Поиск числа в файле';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(FileScaner $fileScaner)
    {
        $this->info($this->description);
        $params = $this->valiateInput($this->options());
        try {
            $res = $fileScaner->setFile($params['file-name'])
                    ->scan($params['scan-for']);
            $storeFileName =  md5($params['file-name'] . microtime()) . 
                    '.' . File::extension($params['file-name']);
            $scanResult = new ScanResult();
            $scanResult->file_name_source = $params['file-name'];
            $scanResult->file_name = $storeFileName;
            $scanResult->result = serialize($res);
            $scanResult->needle = $params['scan-for'];
            $storeFileName = storage_path('app/' . ScanResult::FOLDER_FILE . '/') . 
                    $storeFileName;
            if (File::copy($params['file-name'], $storeFileName) && $scanResult->save()) {
                $this->info('Поиск завершён успешно.');
            } else {
                $this->info('Ошибка сохранения результатов');
            }            
        } catch (\Exception $e) {
            $this->line($e->getMessage());
        }
        $this->info('Результат поиска:');
        if (!empty($res)) {
            $this->line(implode(',', $res));
        } else {
            $this->line('Ничего не найдено.');
        }
    }
    
    
    private function valiateInput(array $params)
    {
        $validate = $this->getValidator($params);
        if ($validate->fails()) {
            if ($validate->errors()->has('scan-for')) {
                $this->line("(Ctrl+C) завершение");
                $params['scan-for'] = $this->ask('Число от 1 до 9 для поиска:');
            }
            if ($validate->errors()->has('file-name')) {
                $this->line("(Ctrl+C) завершение");
                $params['file-name'] = $this->ask('Полний путь к файлу для поиска числа:');
            }            
            $validate = $this->getValidator($params);
        }        
        while ($validate->fails()) {
            if ($validate->errors()->has('scan-for')) {
                $this->showErrors($validate->errors()->get('scan-for'));
                $this->line("(Ctrl+C) завершение");
                $params['scan-for'] = intval($this->ask('Число от 1 до 9 для поиска:'));
                $validate->setData($params);
                continue;
            }
            if ($validate->errors()->has('file-name')) {
                $this->showErrors($validate->errors()->get('file-name'));
                $this->line("(Ctrl+C) завершение");
                $params['file-name'] = $this->ask('Полний путь к файлу для поиска числа:');
            }            
            // закоментировано из-за проблем использования повторно экземпляра валидатора
            // $validate->setData($params);
            $validate = $this->getValidator($params);
        }
        return $params;
    }
    
    /**
     * Вывод в консоль сообщей об ошибках
     * 
     * @param array $errors
     */
    private function showErrors(array $errors)
    {
        foreach ($errors as $error) {
                $this->error($error);
        }        
    }
    
    
    private function getValidator( array $params) : Validator
    {
        return  Vld::make($params, 
                [
                    'scan-for' => 'required|filled|numeric|min:1|max:9',
                    'file-name' => [
                        'required',
                        function ($attribute, $value, $fail) {
                            if (!File::exists($value) || !File::isFile($value)) {
                                $fail('Файл "' . $value . '" не найден');
                            }
                        }
                       ],
                 ]);
    }
}
