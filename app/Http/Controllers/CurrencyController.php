<?php

namespace App\Http\Controllers;

use App\Services\Currency\CurrencyReaderInterface;
use App\Http\Requests\CurrenctFormData as FormRequest;

class CurrencyController extends Controller
{
    public function index()
    {         
        return view('currency.index');
    }
    
    public function exchange(FormRequest $request, CurrencyReaderInterface $reader)
    {
        try {
            $currency = $reader->currency();
        } catch (\Exception $e) {
            return view('currency.index', ['errorMessage' => $e->getMessage()]);
        }
        
        $data = $request->validated();
        $valute = $currency[$data['currency']];
        try {
        $calc = app()->makeWith('Calc', 
                [ 
                    'value' => (float)$valute['value'], 
                    'nominal' => $valute['nominal'], 
                    'amount' => (float)$data['amount']
                ]);
        } catch (\Exception $e) {
            return view('currency.index', ['errorMessage' => $e->getMessage()]);
        }
        
        return view('currency.index', [
            'amount' => $request->input('amount'),
            'currency' => $request->input('currency'),
            'valute' => $valute,
            'rub' => $calc->calcFormat('Руб.')
        ]);
    }
}
