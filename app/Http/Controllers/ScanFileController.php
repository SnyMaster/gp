<?php

namespace App\Http\Controllers;

use App\Services\Scan\FileScanerNumber;
use App\Services\Models\ScanResult;
use Illuminate\Support\Facades\File;
use App\Http\Requests\ScanFileForm;
use App\Services\Scan\FileScaner;

class ScanFileController extends Controller
{
    public function index()
    {
        return view('scan.index');
    }
    
    public function scan(ScanFileForm $request, FileScaner $fileScaner)
    {
        try {
            $res = $fileScaner->setFile($request->filename->path())
                    ->scan($request->input('scanfor'));
        } catch (\Exception $e){
            report($e);
            return false;
        }
        
        try{
            $storeFileNameBase =  md5('formload' . microtime()) . 
                    '.' . $request->filename->extension();                    
            if (!File::copy($request->filename->path(), storage_path('app/' . ScanResult::FOLDER_FILE . '/') . 
                    $storeFileNameBase) ) {
                throw new \Exception('Ошибка сохранения результата');
            }           
        } catch (\Exception $e) {
            //  report($e);
        } 
        
        try{
            $scanResult = new ScanResult();
            $scanResult->file_name_source = 'formload';
            $scanResult->file_name = $storeFileNameBase;
            $scanResult->result = serialize($res);
            $scanResult->needle = $request->input('scanfor');
            $scanResult->save();
        } catch (\Illuminate\Database\QueryException $e) {
            //  report($e);
        } catch (\Exception $e) {
            //  report($e);
        } 
        return view('scan.index', ['result' => $res]);
    }
    
}
