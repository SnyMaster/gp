<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurrenctFormData extends FormRequest
{
    protected $reader;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reader = app('App\Services\Currency\CurrencyReaderInterface');

        return [
            'amount' => 'bail|required|numeric|min:1',
            'currency' => [
                'bail',
                'required', 
                'string',
                'size:3', 
                function($attr, $value, $fail) use ($reader){
                        try {
                            $currency = $reader->currency();
                        } catch (\Exception $e) {
                            $fail($e->getMessage());
                        }
                        if (isset($currency) && !array_key_exists($value, $currency)) {
                            $fail('Указанный код валюты "'. $value . '" не найден');
                        }
                    }
                ],
        ];
    }
    
    public function attributes(): array {
        return [
            'amount' => 'Сумма для обмена',
            'currency'=> 'Код валюты',
        ];
    }

}
