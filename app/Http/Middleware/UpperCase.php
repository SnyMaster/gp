<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class UpperCase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  $fields список полей для обработки 
     *          поля в списке через запятутую
                ->middleware('uppercase:field1,field2,field3')
     * 
     * @return mixed
     */
    public function handle($request, Closure $next, string ...$fields)
    {
            $params = $request->all();
            array_walk($params, function(&$elem, $key) use ($fields){ 
                $elem = !empty($fields) ? (in_array($key, $fields) ? Str::upper($elem) : $elem) : $elem;     
            });
            $request->replace($params);

        return $next($request);
    }
}
