<?php

namespace App\Http\Middleware;

use Closure;

class RoundCurrency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @params string $fileds список полей для обработки 
     *          поля в списке через запятутую
     *         ->middleware('roundcurrency:field1,field2,field3')
     * 
     * @return mixed
     */
    public function handle($request, Closure $next, string ...$fields)
    {
        $params = $request->all();
        array_walk($params, function(&$elem, $key) use ($fields){ 
            $elem = is_numeric($elem) ? 
                    (!empty($fields) ? (in_array($key, $fields) ? round($elem, 2) : $elem) : $elem) : 
                    $elem;     
        });
        $request->replace($params);
        
        return $next($request);
    }
}
