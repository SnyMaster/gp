<?php

namespace App\Services\Models;

use Illuminate\Database\Eloquent\Model;

class ScanResult extends Model
{
    const FOLDER_FILE = 'scan';
    
    protected $table = 'scan_result';
    protected $primaryKey = 'scan_result_id';
}
